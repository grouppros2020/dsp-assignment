import ballerina/grpc;
import ballerina/io;

aSong song_1 = {
date: "22/10/2020",
artists: [
	{
	   name: "Winston Marshall",
	   member: "yes"
	},
	{
	   name: "Ben Lovett",
	   member: "yes"
	},
	{
	   name: "Baaba Maal",
	   member: "no"
	}
],
band: "Mumford & Sons",
songs: [
	{
		title: "There will be time",
		genre: "folk rock",
		platform: "Deezer"
	}
]
};

public function main (string... args) returns error?{

    cali_StorageBlockingClient ep = new("http://localhost:9090");
	
	var userInput = io:readln("Enter User Input : ");
	
	if(userInput == "1"){
		[Key_Version, grpc:Headers] result = check ep->writeRecord(song_1);
		io:println("HASH KEY : ", result[0].songKey );
		io:println("VERSION  : ", result[0].songVersion );
		
	}else if(userInput == "2"){
		var key_ = io:readln("Enter User Input : ");
		var version_ = io:readln("Enter User Input : ");
		
		Key_Version serverResponse = {
			songKey: key_,
			songVersion: 1

		};
		
		[aSong, grpc:Headers] result = check ep->updateRecord(serverResponse);
		
	}else if(userInput == "3"){
	
	}else{
		io:println(" INCORRECT INPUT");
	
	}

}


