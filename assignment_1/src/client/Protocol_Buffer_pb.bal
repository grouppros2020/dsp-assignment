import ballerina/grpc;

public type cali_StorageBlockingClient client object {

    *grpc:AbstractClientEndpoint;

    private grpc:Client grpcClient;

    public function __init(string url, grpc:ClientConfiguration? config = ()) {
        // initialize client endpoint.
        self.grpcClient = new(url, config);
        checkpanic self.grpcClient.initStub(self, "blocking", ROOT_DESCRIPTOR, getDescriptorMap());
    }

    public remote function writeRecord(aSong req, grpc:Headers? headers = ()) returns ([Key_Version, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("service.cali_Storage/writeRecord", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<Key_Version>result, resHeaders];
        
    }

    public remote function updateRecord(Key_Version req, grpc:Headers? headers = ()) returns ([aSong, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("service.cali_Storage/updateRecord", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<aSong>result, resHeaders];
        
    }

    public remote function viewRecord(Key_Version req, grpc:Headers? headers = ()) returns ([aSong, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("service.cali_Storage/viewRecord", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<aSong>result, resHeaders];
        
    }

};

public type cali_StorageClient client object {

    *grpc:AbstractClientEndpoint;

    private grpc:Client grpcClient;

    public function __init(string url, grpc:ClientConfiguration? config = ()) {
        // initialize client endpoint.
        self.grpcClient = new(url, config);
        checkpanic self.grpcClient.initStub(self, "non-blocking", ROOT_DESCRIPTOR, getDescriptorMap());
    }

    public remote function writeRecord(aSong req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("service.cali_Storage/writeRecord", req, msgListener, headers);
    }

    public remote function updateRecord(Key_Version req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("service.cali_Storage/updateRecord", req, msgListener, headers);
    }

    public remote function viewRecord(Key_Version req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("service.cali_Storage/viewRecord", req, msgListener, headers);
    }

};

public type Songs record {|
    string title = "";
    string genre = "";
    string platform = "";
    
|};


public type Artists record {|
    string name = "";
    string member = "";
    
|};


public type aSong record {|
    string songKey = "";
    int songVersion = 0;
    string date = "";
    Artists[] artists = [];
    string band = "";
    Songs[] songs = [];
    
|};


public type Key_Version record {|
    string songKey = "";
    int songVersion = 0;
    
|};



const string ROOT_DESCRIPTOR = "0A1550726F746F636F6C5F4275666665722E70726F746F120773657276696365224F0A05536F6E677312140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D22350A074172746973747312120A046E616D6518012001280952046E616D6512160A066D656D62657218022001280952066D656D62657222BD010A0561536F6E6712180A07736F6E674B65791801200128095207736F6E674B657912200A0B736F6E6756657273696F6E180220012803520B736F6E6756657273696F6E12120A0464617465180320012809520464617465122A0A076172746973747318042003280B32102E736572766963652E4172746973747352076172746973747312120A0462616E64180520012809520462616E6412240A05736F6E677318062003280B320E2E736572766963652E536F6E67735205736F6E677322490A0B4B65795F56657273696F6E12180A07736F6E674B65791801200128095207736F6E674B657912200A0B736F6E6756657273696F6E180220012803520B736F6E6756657273696F6E32AD010A0C63616C695F53746F7261676512330A0B77726974655265636F7264120E2E736572766963652E61536F6E671A142E736572766963652E4B65795F56657273696F6E12340A0C7570646174655265636F726412142E736572766963652E4B65795F56657273696F6E1A0E2E736572766963652E61536F6E6712320A0A766965775265636F726412142E736572766963652E4B65795F56657273696F6E1A0E2E736572766963652E61536F6E67620670726F746F33";
function getDescriptorMap() returns map<string> {
    return {
        "Protocol_Buffer.proto":"0A1550726F746F636F6C5F4275666665722E70726F746F120773657276696365224F0A05536F6E677312140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D22350A074172746973747312120A046E616D6518012001280952046E616D6512160A066D656D62657218022001280952066D656D62657222BD010A0561536F6E6712180A07736F6E674B65791801200128095207736F6E674B657912200A0B736F6E6756657273696F6E180220012803520B736F6E6756657273696F6E12120A0464617465180320012809520464617465122A0A076172746973747318042003280B32102E736572766963652E4172746973747352076172746973747312120A0462616E64180520012809520462616E6412240A05736F6E677318062003280B320E2E736572766963652E536F6E67735205736F6E677322490A0B4B65795F56657273696F6E12180A07736F6E674B65791801200128095207736F6E674B657912200A0B736F6E6756657273696F6E180220012803520B736F6E6756657273696F6E32AD010A0C63616C695F53746F7261676512330A0B77726974655265636F7264120E2E736572766963652E61536F6E671A142E736572766963652E4B65795F56657273696F6E12340A0C7570646174655265636F726412142E736572766963652E4B65795F56657273696F6E1A0E2E736572766963652E61536F6E6712320A0A766965775265636F726412142E736572766963652E4B65795F56657273696F6E1A0E2E736572766963652E61536F6E67620670726F746F33"
        
    };
}

